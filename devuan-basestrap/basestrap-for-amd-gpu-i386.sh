#!/bin/bash

sudo apt-get update && sudo apt-get full-upgrade && sudo apt-get dist-upgrade

sudo apt-get install --no-install-recommends va-driver-all:i386 mesa-vdpau-drivers:i386 mesa-va-drivers:i386 mesa-vulkan-drivers:i386 libglx-mesa0:i386 libgl1-mesa-dri:i386 libpulse0:i386 pipewire:i386 libosmesa6:i386 libgl1:i386

sudo apt-get autoclean -y && sudo apt-get autoremove -y