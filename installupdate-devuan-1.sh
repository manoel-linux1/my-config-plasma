#!/bin/bash

clear

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

clear

sudo apt-get remove xserver-xorg-video-intel

sudo apt-get remove xserver-xorg-video-nouveau

sudo apt-get remove xserver-xorg-video-amdgpu

sudo apt-get remove xserver-xorg-video-ati

sudo apt-get remove xserver-xorg-video-vesa

sudo apt-get remove xserver-xorg-video-fbdev

sudo apt-get update && sudo apt-get full-upgrade && sudo apt-get dist-upgrade

sudo apt-get install --no-install-recommends vulkan-tools steam-devices

sudo apt-get update && sudo apt-get full-upgrade && sudo apt-get dist-upgrade

sudo apt-get install --no-install-recommends qtwayland5 qt6-wayland breeze-gtk-theme p7zip xarchiver xdg-user-dirs xsettingsd xdg-utils flatpak xdg-desktop-portal xdg-desktop-portal-gtk kde-standard

sudo apt-get update && sudo apt-get full-upgrade && sudo apt-get dist-upgrade

sudo apt-get install plasma-workspace-wayland qml-module-org-kde-kcm qml-module-org-kde-kitemmodels systemsettings kscreen kde-config-screenlocker

sudo apt-get remove pulseaudio pulseaudio-utils

flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

clear

echo "#################################################################"

sudo rm -rf /usr/bin/installupdate-kernel-custom

sudo rm -rf /usr/bin/installupdate-waydroid

sudo rm -rf /usr/bin/mini-opti

sudo rm -rf /usr/bin/opti-kernel

sudo rm -rf /usr/bin/opti-wayfire

sudo rm -rf /usr/bin/startwayfire

sudo rm -rf /usr/bin/update-devuan

sudo rm -rf /usr/bin/wayfire-tweaks

sudo rm -rf /etc/sysctl.conf

sudo rm -rf /etc/xdg/autostart/pipewire.desktop

sudo rm -rf /etc/xdg/autostart/pipewire-pulse.desktop

sudo rm -rf /etc/xdg/autostart/wireplumber.desktop

echo "#################################################################"

clear

echo "#################################################################"

sudo cp installupdate-kernel-custom /usr/bin/

sudo cp installupdate-waydroid /usr/bin/

sudo cp mini-opti /usr/bin/

sudo cp opti-kernel /usr/bin/

sudo cp opti-wayfire /usr/bin/

sudo cp startwayfire /usr/bin/

sudo cp update-devuan /usr/bin/

sudo cp wayfire-tweaks /usr/bin/

sudo cp sysctl.conf /etc/

sudo cp -r autostart/* /etc/xdg/autostart/

echo "#################################################################"

clear

echo "#################################################################"

sudo chmod +x /usr/bin/installupdate-kernel-custom

sudo chmod +x /usr/bin/installupdate-waydroid

sudo chmod +x /usr/bin/mini-opti

sudo chmod +x /usr/bin/opti-kernel

sudo chmod +x /usr/bin/opti-wayfire

sudo chmod +x /usr/bin/startwayfire

sudo chmod +x /usr/bin/update-devuan

sudo chmod +x /usr/bin/wayfire-tweaks

echo "#################################################################"

clear

cd $HOME

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(SYSCTL)"
echo "#################################################################"
cat /etc/sysctl.conf